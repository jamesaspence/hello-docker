FROM node:8.6.0

USER node
ENV HOME=/home/node
ARG APP_HOME=$HOME/test

RUN mkdir -p $APP_HOME/node_modules
COPY ./package.json $APP_HOME/package.json
COPY ./yarn.lock $APP_HOME/yarn.lock
WORKDIR $APP_HOME
RUN yarn --silent