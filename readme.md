# Hello Docker

A playground to test Docker and its configurations

## Goals

- Node JS instance with configurable ENV variables
- ENV variables to DB instance
- ~~Docker compose configuration that spins up services for Node and DB~~
- Connection between DB and Node service
- Easy watching / refreshing of code (i.e. webpack services)
- Persistent data / code changes via named volumes

## Prerequisites

- Docker
- NPM/Yarn (Yarn used in the following examples)

## Running

First time run:
```bash
yarn run init
```

Followup runs
```bash
# brings containers up
yarn run up
# Brings them down
yarn run down
```

Want to destroy?
```bash
yarn run destroy
```

Something screwy? Want to refresh everything?
```bash
yarn run reset
```