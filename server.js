const express = require('express');
const http = require('http');

const app = express();
const server = http.createServer(app);

app.get('/', (req, res) => res.send(`Made it!`));

const port = process.env.PORT;
server.listen(port, () => console.log(`server running on port '${port}'.`));
